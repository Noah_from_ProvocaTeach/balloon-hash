module BalloonHash

export balloon_hash, 
       balloon_256,
       balloon_384

using SHA
using Random123

struct HashedPassword
  digest::String
  salt::String
end

# Grabs the previous index in an array. Helper function.
prev_index(len::Integer, ind::Integer) = ind == 1 ? len : ind - 1

"""
  `prehash(c::Char)`
  `prehash(str::String)`
Converts a string or character into an array of `UInt8` values (bytes).
"""
prehash(c::Char) = collect(codeunits(string(c)))
prehash(str::String) = collect(codeunits(str))
prehash(n::Integer) = prehash(Char(n))

function balloon_hash(passwd, salt, space::Integer, time::Integer, hash_f::Function, delta = 7)
  # Step 0: Initialize array.
  count = 0
  hashf_vcat(arrays...) = hash_f(vcat(arrays...))

  # Step 1: Expand input into array.
  initial_hash = hash_f( Char(count) * passwd * salt )
  count += 1
  blox = Array{UInt8}(undef, length(initial_hash), space)
  blox[:,1] = initial_hash
  for s in 2:space
    blox[:,s] = hashf_vcat(prehash(count), blox[:,s-1] )
  end

  # Step 2: Mix array contents.
  for t in 1:time
    for s in 1:space
      # Step 2a: Hash each block with its preceding block.
      blox[:,s] = hashf_vcat(blox[:,s], blox[:, prev_index(space, s)])
      # Step 2b: Hash in pseudorandomly chosen blocks
      for i in 1:delta
        seed_array = sha3_256(
          vcat(prehash(count), prehash(salt), prehash(t), prehash(s), prehash(i))
        )
        seed1, seed2 = reinterpret(UInt128, seed_array)
        rng = Philox4x(UInt64, (seed1, seed2))
        random_index = rand(rng, Int64)
        other = 1 + mod(random_index, space)
        blox[:,s] = hashf_vcat(blox[:,s], blox[:,other])
      end
    end
  end

  # Step 3: Return output
  return blox[:, space]
end

#= function hash_password(passwd)

end =#

function balloon_256(passwd, salt, space=48, time=12, delta=7)
  balloon_hash(passwd, salt, space, time, sha3_256)
end

function balloon_384(passwd, salt, space=48, time=12, delta=7)
  balloon_hash(passwd, salt, space, time, sha3_384)
end

end